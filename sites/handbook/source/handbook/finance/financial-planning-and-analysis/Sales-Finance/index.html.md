---
layout: handbook-page-toc
title: "Sales Finance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Welcome to the Sales Finance Handbook!

## Common Links

  * [Professional Services (PS) Finance](/handbook/finance/financial-planning-and-analysis/Sales-Finance/Professional-Services-Finance)

### Finance Business Partner Alignment

| Name | Function |
| -------- | ---- |
| @fkurniadi | Overall |
| @alcurtis | Enterprise Sales, Channels, Alliances |
| @ysun3 | Commercial Sales, Customer Success, Professional Services, Field Ops |

## Sales Forecast Rhythm
We believe an excellent forecasting process enables us to deploy our resources effectively, risk-manage the business, and provide early warning systems. At GitLab, we design our Sales Forecast Rhythm to foster careful inspection and execution of bookings target throughout the quarter. Each week we review various aspects of the business, such as Current/Next Quarter pipeline, Renewals timing, and leading indicators KPIs, to name but a few.

<figure class="video_container">
<iframe src="https://docs.google.com/spreadsheets/d/e/2PACX-1vStHiw-vsSJXuWFkB-iZ37wZXI1GXdI1bQpTin5TfU6v1_PWMFgSjxuttzgCqUeucagCiLPjJAmOKkq/pubhtml?widget=true&amp;headers=false"></iframe>
</figure>

## Pipeline Velocity
This file details how much Net IACV has been closed relative to the quarterly target and the pace it has been booked at throughout the quarter.  The current quarter performance is 
compared to other historical quarters, to highlight whether the speed at which the net iACV has been closed is ahead or behind that of historical performance. 

In addition, the file also analyses how open pipeline is progressing for the next quarter relative to its quarterly target and the pace at which new opportunities are being added. This is 
also compared to historical quarters to inform whether the current quarter is ahead, in line, or behind the pace of previous quarters.

[File](https://docs.google.com/spreadsheets/d/1O81k_XpInMqn_pLPdbPJruh_nq0QQthJ9-nOiXZzBf0/edit#gid=2079563316)

## LAM Dashboard
This dashboard analyzes the Landed Addressable Market (LAM) offered by our existing customer base

[README](https://docs.google.com/presentation/d/1OfxlJb1F-dndEQZ2zozppGJm1J6CyxN79MfABeh5M-g/edit#slide=id.g8fb2a2c968_1_77)

[SFDC Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oXOd)

## Net IACV Pipeline Movement
This file is intended to detail how the pipeline evolves throughout the quarter: capturing deals in the initial pipeline, those added later in the quarter, closed deals, slipped deals, and those remaining at quarter end.

[File](https://docs.google.com/spreadsheets/d/1L4Rl6hGb5t8x8f_3ILwUlYvOL7Rdo2Rh8FZf0RumBqE/edit#gid=1993507993)
