---
layout: handbook-page-toc
title: "GitLab BV (Belgium) Benefits"
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-operations).

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

## Specific to Belgium Based Employees

Belgian employees are on state benefits. This includes sickness, medical, disability, pension, and unemployment. For more information, please take a look at the corresponding government website.

GitLab does not plan on offering additional private medical or pension benefits at this time due to the government cover.

GitLab is currently reviewing adding a Life Insurance policy for team members in Belgium.


## Ecocheques

Each year all employees will be sent ecocheques no later than July 31. In accordance with the local law these are provided by GitLab at no cost to the individuals. These are currently sent by GitLab's payroll provider in electronic form via a [Sodexo](http://be.benefits-rewards.sodexo.com/) card. A pin for this card will be sent separately. The maximum value is 250 Euros, the amount that will be granted is pro-rated depending on the employee's start date. The reference period is from July 1 of the previous year to June 30 of the year of payment. These ecocheques must be used to purchase ecological products and services; you can always find a current list of what is available on the [National Labor Council](http://www.cnt-nar.be/CAO-ORIG/cao-098-quinquies-(23-05-2017).pdf) website. These ecocheques are exempt from social security contributions and may not be awarded for replacement or conversion of salary.

## GitLab B.V. Belgium Leave Policy

* Statutory Parental Leave
  - Maternity leave is 15 weeks, which can be split in prenatal leave, one week before the delivery date. Postnatally leave is at least nine weeks, allowing the mother to choose five weeks, either as prenatal as postnatally.
  - Dads are given 10 days, three of which are 100% paid. The remaining seven can be used during the baby's first four months.
    * To process a paternity leave, GitLab will notify the payroll provider who will conversely update the pay code and notify the social office.
  - Parental leave should be requested by the employee and registered by three months before the start of leave.
  * Statutory Vacation Leave
  - Full time employees are entitled to 20 statutory vacation days per year. If a team member leaves GitLab, the company will prepare a vacation certificate with the number of unused vacation days in the current year and early vacation pay for the vacation days that were already accruied for the following year. The employee can then hand over these documents to their next employer to determine vacation entitlements. Similarly, GitLab requires the vacation certificate from new team member's previous employer to ensure vacation pay is properly calculated and added to June payroll.<br>


***Note: GitLab has a global [`no ask time off policy`](/handbook/paid-time-off/#paid-time-off), which is the guideline for all GitLab employees globally. We highlight statutory requirements for some countries as well to insure local compliance, but stress that our Paid Time Off policy is applicable for all GitLab team members globally irrespective of local policy.*** 

People Ops will consult with Vistra to ensure that the statute is met.
