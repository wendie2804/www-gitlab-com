---
layout: handbook-page-toc
title: "Create:Editor Backend Engineer Internship"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
The **Create:Editor Team** internship is the result of [The Engineering Internship Pilot Program](/handbook/engineering/internships/) that started at the end of 2019.  The ultimate goal of this program is to transform an entry-level candidate into an Individual Contributor who could meet the requirements for a [Junior Engineer](https://about.gitlab.com/job-families/engineering/backend-engineer/#junior-backend-engineer).

## OKRs for the first 30 days

The OKRs for the first 30 days of the Internship Program are:

**Objective**: **Quickly train the intern to contribute to the team's overall MR count** 
*  Key Result: Intern MR count increasing by 1 each week
*  Key Result: Intern is able to independently complete 1 issues from our issue board

**Objective**: **Expand the Interns Network** 
*  Key Result: Intern completes 10 coffee chats with GitLab Team Members outside of the Internship Program
*  Key Result: Intern invites at least 10 GitLab Team Members to their LinkedIn Network 

**Objective**: **Quickly train the intern on GitLab Products** 
*  Key Result: Intern creates at least 15 Handbook Updates within the first month at GitLab using the Web IDE
*  Key Result: Intern creates a Demonstration Video of how to use Snippets and makes it available to peers in the intern program
*  Key Result: Intern creates an Employee Development Personal Project and populates it with Issues
 
## Roles

For this program to be successful, the Roles and Responsibilities must be transparent to the Internship Program participants. The primary roles of the program include:

* [Engineering Manager](https://about.gitlab.com/job-families/engineering/backend-engineer/#backend-manager-engineering) - Manager
* [Senior Backend Engineering](https://about.gitlab.com/job-families/engineering/backend-engineer/#senior-backend-engineer) - Mentor
* Intern - Participant

## Responsibilities


### Manager Responsibilities

**Prior to the Start of the Internship:**
* Engineering Manager and Mentor Internship Kick Off Meeting
* Create a backlog of small issues and assign to the Mentor, Engineering Manager and the Intern

**During the first week:**
* Schedule Weekly Meeting (no need to record these meetings)
* Walk through various team processes
* Set up at least one Weekly Coffee Chat for the Intern with different team members
* Work with the intern to complete the Gap Analysis

**Daily:**
* Review / Respond to the Intern Daily Check In
* Monitor the Editor Intern Slack Channel
* Serve as resource to the Intern for answering technical questions 

**Weekly**
* Weekly 1-1:
     * Agenda:
          * Discuss Outstanding Questions
          * Review Status of current Merge Requests
          * Review Status of current Issues
          * Review Status of Employee Project
    * Schedule one asynchronous 1:1
    * Schedule one synchronous 1:1
    * Assign at least 1 Handbook Update Merge Request to the interns backlog
    * Review the Intern's calendar weekly, to ensure priorities are in sync with the EMs expectations

**As needed:**
* Create a backlog of small issues and assign to the Mentor, Engineering Manager and the Intern
* Record as many meetings as possible with the Intern so that they can use these discussions to review information


### Mentor Responsibilities

The Intern will have a Primary Mentor, however any of these responsibilities can be delegated to other team members.

**Prior to the Start of the Internship:**
* Engineering Manager and Mentor Internship Kick Off Meeting
* Create a Backlog of small issues and assign to the Mentor, Engineering Manager and the Intern

**Daily:**
* Monitor the Editor Intern Slack Channel
* Serve as resource to the Intern for answering technical questions 

**During the first week:**
* Schedule Weekly Meeting (no need to record these)
* Walk through the development process from start to finish with the Intern
* Walk through how we use labels on our issue boards with the intern

**Weekly**
* Participate in at least one pair programming session
* Weekly 1-1:
     * Agenda:
          * Discuss Outstanding Questions
          * Discuss Weekly Backlog Refinement
          * Review Status of current Merge Requests
          * Review Status of current Issues
          * Review Status of Employee Project

**As needed:**
* Create a backlog of small issues and assign to the Mentor, Engineering Manager and the Intern
* Identify best issues to Pair Program with the Intern
* Record as many meetings as possible with the Intern so that they can use these discussions to review information

### Intern Responsibilities

**During the first week:**
* Review Editor Video Library
     * Internship Overview
     * Weekly 1-1 Formats
     * How we use Labels
     * You and Your Mentor
     * Ask Questions
     * Learn about Our Features
     * Communicating
* Follow the Internship Day to Day Activities
* Create and Maintain an Employee Development Project
* Work with the Engineering Manager to complete the Gap Analysis

**Weekly:**
* Pre Populate the asynchronous 1:1 prior to the meeting
* Prioritize your work for the week and add to Google Calendar  
* Participate in all team meetings and activities
* Attend Coffee Chats with various GitLab Team Members

**Daily:**
* Perform a Daily Check In through Slack to the Editor_Intern_Slack private channel
     * What did you do yesterday?
     * What do you plan to do today?
     * Is anything blocking your work?
* Complete MRs / Issues

### Gap Analysis Template

A Gap Analysis will be conducted during the first week of the internship.  The Gap Analysis Template will include the following columns:

* Backend Engineer Responsibilities
* Desired State
* Current State
* SMART Goals
* Iterative Steps (issues)


| **Backend Engineer Responsibilities** | **Desired State** |  **Current State** |  **SMART Goals** |  **Iterative Steps (issues)**| 
| ------ | ------ | ------ | ------ | ------ |  
| `To be completed by Supervisor`  | `To be completed by Supervisor and Mentor`  | `To be completed by Intern`  | `To be completed by Supervisor and Mentor`  | `To be completed by Intern`  |   
|   |   |   |   |   |  

**Backend Engineer Responsibilities**

* The content for this column will be retrieved from the [Backend Engineer Role](https://about.gitlab.com/job-families/engineering/backend-engineer/#intermediate-requirement) responsibilities and requirements.
 
**Desired State**

* This column represents the skill level the intern should have by the end of the internship.

**Current State**

* This column represents the skill level the intern has at the start of the internship.

**SMART Goals**

* This column will contain SMART Goals used to track and measure progress and accomplishments made during the internship.
     * **S**pecific (so you know exactly what you are trying to achieve)
     * **M**easurable (so you know when you have achieved it)
     * **A**ction-oriented (so you can DO something about it)
     * **R**ealistic (so it IS achievable) and 
     * **T**ime-Bound (has a deadline)


**Iterative Steps ([issues](https://docs.gitlab.com/ee/user/project/issues/))**

* This column will contain the Iterative Steps that need to be taken to meet the SMART goals.  These goals will be documented as issues. Consistent with GitLab's [iteration](/handbook/values/#iteration) value, each step should be written so that it takes no more than two to three days to complete.