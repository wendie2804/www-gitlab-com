---
layout: handbook-page-toc
title: "Competitive Intelligence Resources"
noindex: true
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

**Welcome to the new competitive intelligence resources page!**  

Here are some quick tips about the content on this page.
- Read the one slide summary of the content if you are time constrained or need a quick refresher.
- Content is accessible only by GitLab personnel; an exception to GitLab's transparency policy due to business sensitive content.
- For competitive research requests, please use this [Issue Template](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#).
- Please enter your feedback as comments in the deck.

## Competitors

| DevOps                                                           | Source Code Management                                                     | CI-CD                                          | Security                                               |
|------------------------------------------------------------------|----------------------------------------------------------------------------|------------------------------------------------|--------------------------------------------------------|
| ![GitHub][github-logo] [GitHub](#-github-)                       | ![Bitbucket][bitbucket-logo] [Atlassian-Bitbucket](#-atlassian-bitbucket-) | ![Jenkins][jenkins-logo] [Jenkins](#-jenkins-) | ![Snyk][snyk-logo] [Snyk](#-snyk-)                     |
| ![Azure DevOps][azuredevops-logo] [AzureDevOps](#-azure-devops-) |                                                                            |                                                | ![CheckMarx][checkmarx-logo] [CheckMarx](#-checkmarx-) |
| ![JFrog][jfrog-logo] [JFrog](#-jfrog-)                           |                                                                            |                                                |                                                        |

<!---
Table Source: https://docs.google.com/spreadsheets/d/1Ssdt-Uo7bwPfAxAYedxPP-jxhyfXJDE2mcCjkxN8-zc/edit#gid=0  
-->


-----

## ![GitHub][github-logo] GitHub <a name="github"></a>

**GitHub Competitive Quick Tip:**	GitHub Actions is a new capability and is not yet enterprise ready.

|                         | **Win the Business Decision Makers (BDMs)**                                                                                                                                                                                                                                                                       | **Win the Technical Decision Makers (TDMs)**                                                                                                                                                                                                                                   |
|-------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **Overview & Strategy** | [GitHub Competitive Strategy](https://docs.google.com/presentation/d/16m8cTax_X494Yq_0Sp2RpyYl2OHa_3-rR3-350UmcJI/edit).                                                                                                                                                                                          | [GitHub Competitive Strategy](https://docs.google.com/presentation/d/16m8cTax_X494Yq_0Sp2RpyYl2OHa_3-rR3-350UmcJI/edit).                                                                                                                                                       |
| **Early Stage**         | Coming soon - Post first meeting assets                                                                                                                                                                                                                                                                           | Coming soon - Demos                                                                                                                                                                                                                                                            |
| **Evaluation Stage**    | [GitLab Capabilities missing in GitHub](https://docs.google.com/presentation/d/1WAim4zkwaurifcaJn3bZp31CIzzf8AhnGTcbIdXpug8/edit#slide=id.g6fc8bb1d32_0_190) <br/> <br/> [GitHub-GitLab License Comparison](https://docs.google.com/presentation/d/1xVwIBrsrEIKjfdev9E4T7YaSQT_LVtH_kuex7wqAA_U/edit?usp=sharing) | [GitHub-GitLab CI-CD Comparison](https://docs.google.com/presentation/d/1fDzXpgmvKyLpT0QTsGFnisFUeceNPsaL66vRVJ-sdpg/edit#slide=id.g29a70c6c35_0_68) <br/> <br/> [GitHub Actions Gaps](https://about.gitlab.com/devops-tools/github-vs-gitlab.html#github-actions-gaps) <br/>  |
| **Decision Stage**      | Coming Soon - Objection Handling <br/> <br/> Coming Soon - ROI Drivers & Examples <br/> <br/> [GitLab-GitHub Support Comparison](https://docs.google.com/presentation/d/1-1kAayhvcyMhx3Sn_RxUqX1BGU1P4Y8s4Jf4TjKiwGs/edit#slide=id.g29a70c6c35_0_68)                                                                                          |                                                                                                                                                                                                                                                                                |

----

## ![Jenkins][jenkins-logo] Jenkins <a name="jenkins"></a>

<!--- Table source: https://docs.google.com/spreadsheets/d/1Ssdt-Uo7bwPfAxAYedxPP-jxhyfXJDE2mcCjkxN8-zc/edit#gid=1188948062
-->

**Jenkins Competitive Quick Tip:**	Jenkins is a CI tool. CD capabilities requires plugins and scripts.


|                         | Win the Business Decision Makers (BDMs)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           | Win the Technical Decision Makers (TDMs)                                                                                                                                                                                                                                                                                                                                                                                                                    |
|-------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **Overview & Strategy** | [Customer Profile:](https://docs.google.com/presentation/d/1IvftLfaQyKn5-n1GLgCZokOoLU-FFzQ8LfJ9cf0FVeg/edit#slide=id.g7d21a5666c_0_238Customer)  <br/> <br/> [Customer Personas:](https://docs.google.com/presentation/d/1IvftLfaQyKn5-n1GLgCZokOoLU-FFzQ8LfJ9cf0FVeg/edit#slide=id.g7d44020578_3_0)  <br/> <br/> [Understand Jenkins Pricing Model:](https://about.gitlab.com/devops-tools/jenkins-vs-gitlab.html#pricing)  <br/> <br/> [Quick Tips on How to Compete with Free - i.e. Jenkins Open Source:](https://docs.google.com/presentation/d/1IvftLfaQyKn5-n1GLgCZokOoLU-FFzQ8LfJ9cf0FVeg/edit#slide=id.g7d0f2b9285_0_0) | [Jenkins vs. GitLab Feature Overview:](https://about.gitlab.com/devops-tools/jenkins-vs-gitlab.html#overview) <br/> <br/> [Understand where GitLab is weaker than Jenkins:](https://www.youtube.com/watch?v=3Wr3O6mY5VE&feature=youtu.be) <br/> <br/> [Jenkins Product Offerings - Jenkins, CloudBees, Jenkins X:](https://docs.google.com/presentation/d/1IvftLfaQyKn5-n1GLgCZokOoLU-FFzQ8LfJ9cf0FVeg/edit#slide=id.g7d24fb16f5_1_5)                       |
| **Early Stage**         | [Jenkins BDM Discovery Questions:](https://docs.google.com/presentation/d/1P9GYXeKxQM2wi63IjTqYK2ory1iAWfM5VeMLFi312QI/edit#slide=id.g872129a6f2_0_45) <br/> <br/> [Case Study - How Jenkins slowed down Ticketmaster:](https://about.gitlab.com/blog/2017/06/07/continous-integration-ticketmaster/) <br/> <br/> [Case Study - Hotjar replaces Jenkins:](https://about.gitlab.com/customers/hotjar/)                                                                                                                                                                                                                             | [Jenkins TDM Discovery Questions:](https://docs.google.com/presentation/d/1P9GYXeKxQM2wi63IjTqYK2ory1iAWfM5VeMLFi312QI/edit#slide=id.g872129a6f2_0_36)                                                                                                                                                                                                                                                                                                      |
| **Evaluation Stage**    | [GitLab's Jenkins Migration Services Program:](https://about.gitlab.com/services/migration/jenkins/) <br/> <br/> [Jenkins Importer for Jenkins MIgrations:](https://about.gitlab.com/direction/verify/jenkins_importer/) <br/> <br/> [Jenkins Importer Issue:](https://docs.google.com/document/d/1wEoTjKSjBD7dOVliTYerMu91nRo9nm4hFX_v4AGwRN4/edit)                                                                                                                                                                                                                                                                              | [GitLab Advantages over Jenkins:](https://docs.google.com/presentation/d/1IvftLfaQyKn5-n1GLgCZokOoLU-FFzQ8LfJ9cf0FVeg/edit#slide=id.g7d0f2b9285_0_698) <br/> <br/> [Migrating Customers from Jenkins to GitLab:](https://www.youtube.com/watch?v=RlEVGOpYF5Y&feature=youtu.be) <br/> <br/> [GitLab Docs: Migrating from Jenkins](https://docs.gitlab.com/ee/ci/jenkins/)                                                                                    |
| **Decision Stage**      | [Jenkins focused BDM Objection Handling:](https://docs.google.com/presentation/d/1cs3-UUNb1qrbnHM7ON37_Bm79LKVW6I9wAYXiqHBo4A/edit#slide=id.g872129a6f2_0_45)                                                                                                                                                                                                                                                                                                                                                                                                                                                                     | [Jenkins focused TDM Objection Handling:](https://docs.google.com/presentation/d/1cs3-UUNb1qrbnHM7ON37_Bm79LKVW6I9wAYXiqHBo4A/edit#slide=id.g872129a6f2_0_36 ) <br/> <br/> [Objection Handling follow-up questions:](https://docs.google.com/document/d/1p94F9M2epeebyEwQfIhZDhQ0E2b4iqBPLkR5zbuiceU/edit)  <br/> <br/> [Jenkins Deep Technical Objection Handling:](https://docs.google.com/document/d/18VKkaFRT2wo3JWL8OmHXMNOenl0vNYcUmoNoCsm9wHA/edit#) |

----

## ![Azure DevOps][azuredevops-logo] Azure DevOps <a name="azuredevops"></a>

**Azure Competitive Quick Tip:**	Realizing an End-to-End DevOps Toolchain with Azure DevOps requires the integration of paid for "Add-On" Azure Tools for Security, Automation and Monitoring.


|                         | **Win the Business Decision Makers (BDMs)**                                                                                                                                                                                                                                                                                                                                                                                 | **Win the Technical Decision Makers (TDMs)**                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
|-------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **Overview & Strategy** | [Microsoft Azure DevOps Competitive Strategy](https://docs.google.com/presentation/d/1WUdzaNn-tgm3Pa1W3JQyGjcR9JOR2a0rLUTA7zRNIt8/edit#slide=id.g7dcf5566c6_0_22)  <br/> <br/> [Understand Azure DevOps Pricing](https://about.gitlab.com/devops-tools/azure-devops-vs-gitlab.html#pricing)  <br/> <br/> [Breakdown of Microsoft Azure DevOps Toolchain](https://about.gitlab.com/devops-tools/azure_devops/breakdown.html) | [Microsoft Azure DevOps Toolchain?](https://docs.google.com/presentation/d/1WUdzaNn-tgm3Pa1W3JQyGjcR9JOR2a0rLUTA7zRNIt8/edit#slide=id.g7c8e94f70d_0_21) <br/> <br/> [Microsoft Azure DevOps vs. GitLab Feature Comparison](https://about.gitlab.com/devops-tools/azure-devops-vs-gitlab.html#verify) <br/> <br/> [Azure Battecard Material from Crayon](https://docs.google.com/presentation/d/1hQCzSvmHUfx1XEkAbdHrB0Uyp2kmiha9j0SfY2jFnG4/edit#slide=id.g29a70c6c35_0_68)                                                                                                                      |
| **Early Stage**         | [Competing Against Azure DevOps Toolchain](https://docs.google.com/presentation/d/1WUdzaNn-tgm3Pa1W3JQyGjcR9JOR2a0rLUTA7zRNIt8/edit#slide=id.g7d51831dc1_1_77) <br/> <br/> [Azure DevOps Battlecard](https://docs.google.com/presentation/d/1hQCzSvmHUfx1XEkAbdHrB0Uyp2kmiha9j0SfY2jFnG4/edit#slide=id.g29a70c6c35_0_68)                                                                                                    | [Competing Against Azure DevOps - SCM](https://docs.google.com/presentation/d/1WUdzaNn-tgm3Pa1W3JQyGjcR9JOR2a0rLUTA7zRNIt8/edit#slide=id.g7d56716293_0_180) <br/> <br/> [Competing Against Azure DevOps - CI/CD](https://docs.google.com/presentation/d/1WUdzaNn-tgm3Pa1W3JQyGjcR9JOR2a0rLUTA7zRNIt8/edit#slide=id.g7d5d3581f8_10_133)                                                                                                                                                                                                                                                           |
| **Evaluation Stage**    | [GitLab Migration Services](/handbook/customer-success/professional-services-engineering/offerings/) <br> <br> [Understand How Customers use Azure DevOps](https://docs.google.com/presentation/d/1WUdzaNn-tgm3Pa1W3JQyGjcR9JOR2a0rLUTA7zRNIt8/edit#slide=id.g7c8e94f70d_0_256)                                                                                                                     | [Azure DevOps vs. GitLab Feature Comparison - The Microsoft Perspective](https://docs.microsoft.com/en-us/azure/devops/learn/compare/azure-devops-vs-gitlab) <br> <br> [GitLab's Response to Microsoft's Feature Comparsion for CI/CD](https://about.gitlab.com/direction/verify/continuous_integration/azure_devops_detail.html) <br> <br> [GitLab on Azure Cloud]( https://about.gitlab.com/blog/2016/07/13/how-to-setup-a-gitlab-instance-on-microsoft-azure/#gitlab-on-azure) <br> <br> [Azure Container Registry vs. GitLab](https://about.gitlab.com/devops-tools/azure-cr-vs-gitlab.html) |
| **Decision Stage**      | [Azure Focused Objection Handling](https://docs.google.com/presentation/d/1WUdzaNn-tgm3Pa1W3JQyGjcR9JOR2a0rLUTA7zRNIt8/edit#slide=id.g6dd255414b_4_185)                                                                                                                                                                                                                                                                     |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |

----

## ![JFrog][jfrog-logo] JFrog <a name="jfrog"></a>

**JFrog Competitive Quick Tip:** JFrog Pipelines product lacks enterprise level capabilities such as merge trains, feature flags and review apps.

- [JFrog Pipelines Competitive Overview](https://docs.google.com/presentation/d/16oW884g2sqmWmq8eq4CaGIdGZrZkvM3fjvp8pBGD6Dw/edit#slide=id.g6fc8bb1d32_0_190)
- [JFrog Xray Competitive Overview](https://docs.google.com/presentation/d/1dKqKkOl3R7oTysvLrFw0V35zg_4777WrLx5nYeABYE0/edit#slide=id.g6fc8bb1d32_0_190)
- [JFrog Pricing Analysis](https://docs.google.com/presentation/d/1oD-1JS6ovS0WudCVlSVZMOtxbYU46SRyQvrkffYq-LU/edit#slide=id.g6fc8bb1d32_0_190)

----

## ![Atlassian-Bitbucket][bitbucket-logo] Atlassian-Bitbucket <a name="atlassian-bitbucket"></a>

**Atlassian-Bitbucket Competitive Quick Tip:** Bitbucket is losing market share to other competitors such as GitLab and GitHub.

- [Battlecard](https://docs.google.com/presentation/d/1T9bkAhqCqvbYFX_675iGgA6oO-teRNv1k6guImkCO5c/edit#slide=id.g75defc3d46_6_0)

----

## ![Snyk][snyk-logo] Snyk <a name="snyk"></a>

- [Competitive Strategy Overview](https://drive.google.com/open?id=1Fr_PLIKpbnOQpejo8YNHhOXzsuSbL2uWQ_3UyFtgs4Y)

----

## ![CheckMarx][checkmarx-logo] Checkmarx <a name="checkmarx"></a>

- [Competitive Strategy Overview](https://drive.google.com/open?id=1cfSSAUeevoACf7cohk8spcUumASCHgQi9RrFUSd59Ig)

----

## Other Competitive Content

### [Win-Loss Analysis](https://docs.google.com/presentation/d/1nVsOg-HhNwJXWURZwz4HI0AUaBhSWCiegS-MD-NnoY4)

----

## Helpful Links

### Product Marketing

#### [Version Control and Collaboration UseCase](/handbook/marketing/product-marketing/usecase-gtm/version-control-collaboration/)

#### [Continuous Integration UseCase](/handbook/marketing/product-marketing/usecase-gtm/ci/)

#### [DevSecOps UseCase](/handbook/marketing/product-marketing/usecase-gtm/devsecops/)

#### [Simplify DevOps UseCase](/handbook/marketing/product-marketing/usecase-gtm/simplify-devops/)

#### [GitOps UseCase](/handbook/marketing/product-marketing/usecase-gtm/gitops/)

### Market Research and Customer Insight

#### [Forrester TEI Study](/handbook/marketing/product-marketing/analyst-relations/forrester-tei/)

#### [Requesting a Customer Reference](/handbook/marketing/product-marketing/customer-reference-program/#requesting-a-reference-customer-to-support-a-sales-call)

#### [Analyst Reports to Deepen Your Knowledge](/handbook/marketing/product-marketing/analyst-relations/sales-training/)

#### [Customer Case Studies](https://about.gitlab.com/customers/)
----



[github-logo]: /images/devops-tools/github-logo-small.png "GitHub"
[azuredevops-logo]: /images/devops-tools/azure-devops-logo-small.png "Azure DevOps"
[jfrog-logo]: /images/devops-tools/jfrog-logo-small.png "JFrog"
[atlassian-logo]: /images/devops-tools/atlassian-logo-small.png "Atlassian"
[bitbucket-logo]: /images/devops-tools/bitbucket-logo-small.png "Bitbucket"
[jenkins-logo]: /images/devops-tools/jenkins-logo-small.png "Jenkins"
[snyk-logo]: /images/devops-tools/snyk-logo-small.png "Snyk"
[synopsis-logo]: /images/devops-tools/synopsis-logo-small.png "Synopsis"
[veracode-logo]: /images/devops-tools/veracode-logo-small.png "Veracode"
[checkmarx-logo]: /images/devops-tools/checkmarx-logo-small.png "CheckMarx"
