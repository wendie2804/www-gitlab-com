---
layout: handbook-page-toc
title: "Demandbase"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What is Demandbase
Demandbase is a complete end-to-end solution for [Account-Based Marketing](/handbook/marketing/revenue-marketing/account-based-marketing/). We primarily use Demandbase as a targeting and personalization platform to target online ads to companies that fit our ICP and tiered account criteria. Demandbase also has a wealth of intent data that is available to us through its integration with Salesforce. 

We compile the intent data by building audiences, groups of target accounts with the most potential to purchase based on our buyer criteria, which we can then leverage for use throughout the funnel in advertising and SDR/sales follow-up. Demandbase also delivers ongoing signals around behaviors and intent to keep our list up to date. This information helps both marketing and sales focus our efforts on the right accounts. 

To date, we use the following Demandbase features:
- [ABM platform](https://support.demandbase.com/hc/en-us/sections/360003540751-ABM-Platform-Solution): identify the companies that meet your buying criteria and are showing the right intent signals
- [Targeting](https://support.demandbase.com/hc/en-us/sections/360003540771-Targeting-Solution): plan and implement targeting strategy
- [Self-serve](https://support.demandbase.com/hc/en-us/sections/360009465252-Getting-Started-with-Self-Serve-Targeting): easily set up, launch, and manage campaigns
- [Conversion](https://support.demandbase.com/hc/en-us/sections/360003566352-Conversion-Solution): give SDR and Sales teams the data and insights they need to better understand and target the right individuals, with personalized messages, within their target accounts, so they can close deals faster.
- [ABM analytics](https://support.demandbase.com/hc/en-us/sections/360003540851-ABM-Analytics-Solution): focus on business outcomes and report on account-level reach, engagement, and sales outcomes.
- [Site analytics](https://support.demandbase.com/hc/en-us/sections/360009288811-Site-Analytics-Solution): provide insight around channel performance (i.e. - display advertising, traffic, campaign influence) 

### Demandbase Audiences  
An audience is a list of target accounts that have met our initial criteria.

##### Audience naming convention:
- (AB) Account Based Marketing
- (AC) Acceleration team
- (FM) Field Marketing
- (M) General Marketing (includes growth, alliance, demand gen, etc)
- (S) Sales requests
- (List) General label for audiences that do not fall into another bucket

## Metrics and Reporting
### Demandbase Reporting
#### ABM Metrics
- **Lifted Accounts**: Percentage of the target accounts that have more engagement (page views) during the campaign(s) compared to the baseline period of 30-days prior to the start of the campaign(s). Baseline page view counts are normalized for campaign length in calculating this metric.
- **Page Views:** Total page views on your website during the campaign(s)
- **% increase in Page Views:** Percent change in page views during the campaign(s), compared to baseline period of the 30 days preceding the start of the campaign. *Note: Baseline page view counts are normalized for campaign length in calculating this metric.
- **Account Performance by Stage:**
     - **Total Accounts:** The total number of accounts being targeted
     - **Reach:** he total number of accounts that have been served at least one impression
     - **Visited:** The total number of accounts that have been on site during the campaign(s)
     - **Clicked:** The total number of accounts from which clicks have been generated during the campaign(s)
     - **Engaged:** The total number of targeted accounts that have had three or more unique sessions within a 30-day period.
     - **Converted:** TBD
     - **Opportunity:** The total number of accounts with at least one new CRM opportunity created during the campaign(s)
     - **Won:** The total number of accounts with at least one CRM opportunity that has progressed to Closed/Won during the campaign(s)
#### Digital Campaign Metrics & Performance
- **Impressions:** Total ad views served
- **Clicks:** Total ad clicks
- **CTR:** Click-through rate. -> Formula: Clicks divided by Impressions
- **CPM:** Average cost per one thousand impressions
- **CPC:** Cost per click. -> Formula: Cost divided by Clicks
- Top Publishers
- Top Performing Creative
- Top Engaged Accounts
### Salesforce Reporting
#### Salesforce Metrics
- **[SAO](/handbook/business-ops/resources/#glossary): Sales Accepted Opportunity:** Sales Accepted Opportunity - an opportunity Sales agrees to pursue following an Initial Qualifying Meeting
- **[Closed Won](/handbook/business-ops/resources/#opportunity-stages):** The terms have been agreed to by both parties and the quote has been approved by Finance.

## Demandbase Display Campaigns
Demandbase enables personalized display advertising for target accounts. Demandbase Display ads are different from [the standard Paid Display ads](/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/#display-ads) we typically run in Google Display Network. We partner directly with Demandbase and do not run these through PMG, our paid advertising digital agency.

### Demandbase Campaign Process
If you would like to target named accounts with paid display ads, create an issue with the appropriate Demandbase Campaign Request template:
- [Campaign Request issue template for Field and Corporate Marketing](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/new?issuable_template=Demandbase_Campaign_Request)
- [General Campaign Request issue template (for all other teams)](https://gitlab.com/gitlab-com/marketing/account-based-marketing/-/issues/new?issuable_template=Demandbase_Campaign_Request_Template)

The DMP will then conduct a reach test with Demandbase to determine if your selected accounts are reachable, so please provide estimated budget, campaign duration, geo target, and a list of accounts [formatted for DB (Demandbase)](https://docs.google.com/spreadsheets/d/1sJvmzdhL8k5N6WQPSlE3ndAF0jL8MUTuZcD6dM-1D0w/edit?usp=sharing). If your reach test comes back too low, the DMP will recommend that you add more accounts, expand your geo target, increase your campaign duration, and/or double check to make sure your list is formatted correctly for Demandbase upload. After the reach test confirms reachable accounts and available display inventory, the DMP will send display images and landing page URLs to our DB contacts. Upon receipt, the campaign typically goes live within 1-2 business days.

If your campaign objective is promoting an event or webcast, we recommend launching one month in advance of the event date in order to increase awareness & reach. If your campaign objective is account penetration, we recommend running your campaign as “always-on” to build awareness for your priority accounts and optimize over time with relevant assets. The DMP will provide a UTM report to track inquiries/registrations attributed to your DB campaign. Front-end Demandbase metric reports are also available upon request.

### Demandbase Ads
#### Display Ad Specs
- 728x90
- 300x250
- 160x600
- 300x600
- 970x250
#### Display Ad Copy
- **Top of funnel:** Use a broad value proposition to increase awareness and education, assert your brand value and why prospects should care.
- **Middle of funnel:** Include a tailored value proposition, provide practical how-to content, best practices, and tips and tricks.
- **Bottom of funnel:** Reinforce the value proposition, go into further detail about your offerings with case studies, ROI calculators, and product and solution content.
- **All stages:** Keep it simple and have a clear CTA.
#### Demandbase Campaign Best Practices
##### Campaign Run Time: 
- Event promotion: 1 month minimum
- Target account penetration: 2-3 months minimum
- Longer advertising campaigns that span the full funnel can help build awareness and create more engagement. The result is a higher number of accounts that go into the pipeline with a higher average booking value.
##### Budget:
- [Demandbase funnel budgeting](https://docs.google.com/presentation/d/1swa9gWFAttRAas5feGSWPgsbx1HyUwiNtTXYiOL47wE/edit#slide=id.g810b16a717_0_0)
 - Start with a low monthly budget for top of funnel campaigns and increase the budget as you progress to mid and lower funnel campaigns.
##### Audiences:
- You can segment by stage in the buyer’s journey, company size, industry, etc. The core message for each of these groups is different, therefore they shouldn’t receive the same ad creative.
- Segment audiences into High, Medium, and Low intent based on intent data results and serve assets f








