---
layout: job_family_page
title: "Billing"
---

We’re looking for a billing specialist that can manage the invoicing and collection process in its entirety. Specifically, we’re looking for someone to orchestrate and own the customer billing cycle from quote to cash. This role will collaborate with the sales team and at times with customers.  Attention to detail and an aptitude for accuracy are critical to being successful.

We are a rapidly growing company which means you must be comfortable working in fast paced atmosphere where juggling numerous priorities and deadlines is the norm. We are also a fully distributed team which means you must be self-driven and a highly effective communicator.

## Levels

Read more about [levels](/handbook/hiring/#definitions) at GitLab here.

## Billing Specialist

## Job Grade

The Billing Specialist is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

- Completely own and manage the billing process
- Work closely with the Sales team on various billing related issues, including quote generation
- Assist in cross-functional accounting activities as needed
- Resolve customer inquiries in an accurate, timely manner
- Identify process and system improvements to streamline the revenue cycle
- Communicate process improvements by routinely and frequently updating our handbook and training the sales team
- Financial reporting and analytics of sales figures and A/R data
- Proactively monitor aging reports, following up on delinquent accounts and managing A/R collections
- Ability to take on side projects related to internal initiatives

### Requirements

- Proven ability to fully utilize the Zuora platform from quote to cash collection
- Experience with Salesforce CRM
- Experience billing in a high-volume environment
- Deep understanding of subscription billing
- Superior attention to detail
- Excellent computer skills, self starter in picking up new and complex systems
- Strong knowledge of Google Apps (Gmail, Docs, Spreadsheets,etc).
- Slack is a plus but not necessary
- Bonus points: experience using NetSuite

## Senior Billing Specialist

### Job Grade

The Senior Billing Specialist is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

- Completely own and manage the billing process
- Work closely with the Sales team on various billing related issues, including quote generation
- Assist in cross-functional accounting activities as needed
- Resolve customer inquiries in an accurate, timely manner
- Communicate process improvements by routinely and frequently updating our handbook and training the sales team
- Financial reporting and analytics of sales figures and A/R data
- Proactively monitor aging reports, following up on delinquent accounts and managing A/R collections
- Provide sales and billing reports to upper management as needed
- Apply cash receipts for customer payments and reconcile to monthly bank statements
- Prepare monthly reconciliations between Zuora, Netsuite and Salesforce
- Responsible for training the billing specialist team
- Assist in financial systems upgrades, implementations, testing and maintenance.
- Assist in preparation of sales and VAT tax returns
- Provide sales and billing reports to upper management as needed

### Requirements

- 3+ Years experience with Zuora from quote to cash collection
- Experience with Salesforce CRM
- Experience billing in a high-volume environment
- Deep understanding of subscription billing
- Superior attention to detail
- Excellent computer skills, self starter in picking up new and complex systems
- Strong knowledge of Google Apps (Gmail, Docs, Spreadsheets,etc).
- Bonus points: experience using NetSuite

## Manager, Billing

### Job Grade

The Manager, Billing is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities
- Manages the billing function and the billing team.  Oversees AR processes to ensure timeliness of collections
- Works with the management team to understand the billing function, collections function, the billing team, the billing culture, and the area of strengths/weaknesses, to develop short-term and long-term plans to support the company’s strategic goals.
- Assess current billing operations, offer recommendations for improvement, and implement new processes
- Assists revenue cycle staff with any day to day issues
- Assists in the monthly closing process
- Assist with the annual financial statement audit
- Create and distribute periodic billing metrics and reports; prepare special reports, as requested
- Manages the hiring and training for new hires
- Provide sales and billing reports to upper management as needed
- Collaborate with team members to assist with ad-hoc projects and miscellaneous tasks

### Requirements
- 5+ Years of experience in billing and collections
- 3+ Years experience with Zuora and Salesforce from quote to cash collection
- Proven ability to perform strategic planning and set priorities for a billing department
- Proven track record for improving processes and problem- solving
- Strong leadership skills with an ability to coach and motivate
- Solid understanding of billing and financial concepts
- Strong analytical and problem-solving skills
- Excellent verbal and written communication skills and ability to collaborate with cross-functional teams. Able to work in stressful situations with firm deadlines
- Ability to use GitLab

## Career Ladder

The next step in the Billing job family is to move to the [Controller](/job-families/finance/corporate-controller/) job family.

## Performance Indicators

- [Average days of sales outstanding](/handbook/finance/accounting/#accounts-receivable-performance-indicators)
- [Time for Invoices to be generated when a deal is closed won in Salesforce](/handbook/finance/accounting/#accounts-receivable-performance-indicators)
- [Percentage of ineffective Sox Controls](/handbook/internal-audit/#performance-measures-for-accounting-and-finance-function-related-to-audit)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/)

   * Qualified candidates will be invited to schedule a 30 minute screening call with a member of the Recruitment team
   * Then, candidates will be invited to schedule a 45 minute interviews with the Hiring Manager and a 30 minute interview with a Peer
   * Finally, candidates will be invited to a 45 minute interview with a member of the Executive team.

As always, the interviews and screening call will be conducted via a [video call](https://about.gitlab.com/handbook/communication/#video-calls). See more details about our interview process [here](https://about.gitlab.com/handbook/hiring/interviewing/).
